" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" RUNTIME
" run vim-plug (https://github.com/junegunn/vim-plug) as package manager
call plug#begin('~/.vim/plugged')

" PLUGINS - organized alphabetically by repo name
" camelcasemotion: A vim script to provide CamelCase motion through words
" (fork of inkarkat's camelcasemotion script)
"Plug 'bkad/CamelCaseMotion'
" ctrlp.vim: Full path fuzzy file, buffer, mru, tag, ... finder
Plug 'ctrlpvim/ctrlp.vim'
" greplace: Vim plugin to search and replace a pattern across multiple files interactively
" Forked from yegappan/greplaced and added .gitignore
Plug 'rowoflo/greplace'
"" The right way to use gtags with gutentags
"Plug 'skywind3000/gutentags_plus'
" supertab: Supertab is a vim plugin which allows you to use <Tab> for all your insert completion needs (:help ins-completion)
" Plug 'ervandew/supertab'
"" vimtex: A modern vim plugin for editing LaTeX files
"" tutorial here: https://wikimatze.de/vimtex-the-perfect-tool-for-working-with-tex-and-vim
"Plug 'lervag/vimtex'
"" vim-bufkill: Kill buffer without closing the window or split
"Plug 'qpkorr/vim-bufkill'
" nerdtree: ): The NERDTree is a file system explorer for the Vim editor
Plug 'scrooloose/nerdtree', { 'on': ['NERDTreeFind', 'NERDTreeToggle', 'NERDTreeFocus']}
"" vim-fubitive: Add Bitbucket URL support to fugitive.vim's :Gbrowse command
"Plug 'tommcdo/vim-fubitive'
"" abolish.vim: easily search for, substitute, and abbreviate multiple variants of a word
"Plug 'tpope/vim-abolish'
" vim-commentary: Comment stuff out
Plug 'tpope/vim-commentary'
"" dispatch.vim: Asynchronous build and test dispatcher
"Plug 'tpope/vim-dispatch'
" vim-fugitive: Git wrapper so awesome, it should be illegal
Plug 'tpope/vim-fugitive'
"" vim-jaddy: JSON manipulation and ptree printing
"Plug 'tpope/vim-jdaddy'
"" vim-markdown: Vim Markdown runtime files
"Plug 'tpope/vim-markdown'
"" vim-pathogen: Manage your runtimepath and install plugins with ease
"Plug 'tpope/vim-pathogen'
"" vim-repeat: enable repeating supported plugin maps with .
"Plug 'tpope/vim-repeat'
"" vim-rhubarb: if fugitive.vim is the Git, rhubarb.vim is the Hub
"Plug 'tpope/vim-rhubarb'
" vim-sensible: A universal set of defaults that (hopefully) everyone can agree on
Plug 'tpope/vim-sensible'
""vim-submode: Create your own submodes, e.g. whenever you type |g-| and/or |g+| to undo/redo many times, don't you want to type "g++--++...
"" Forked from kana/vim-submode and added to .gitignore
"Plug 'rowoflo/vim-submode'
"" vim-surround: Quoting/parenthesizing made simple
"Plug 'tpope/vim-surround'
"vim-unimpaired: Pairs of handy bracket mappings
Plug 'tpope/vim-unimpaired'
" vim-airline: Lean & mean status/tabline for vim that's light as air.
Plug 'vim-airline/vim-airline'
" vim-airline-themes: A collection of themes for vim-airline
Plug 'vim-airline/vim-airline-themes'
"" vim-windownswap: Swap windows without ruining your layout
"Plug 'wesQ3/vim-windowswap'
"" A code-completion engine for Vim
" Plug 'ycm-core/YouCompleteMe', { 'do': './install.py' }

"" Maybe later
"" " nerdtree-git-plugin: A plugin for NERDTree showing git status flags
"" Plug 'xuyuanp/nerdtree-git-plugin'
"" " A Vim plugin that manages your tag files
"" Plug 'ludovicchabant/vim-gutentags'
"" " The missing preview window for vim
"" Plug 'skywind3000/vim-preview'

call plug#end()

" SET FILETYPES
" set markdown filetypes
autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown
" set cpp filetypes
autocmd BufNewFile,BufFilePre,BufRead *.cc set filetype=cpp
autocmd BufNewFile,BufFilePre,BufRead *.tpp set filetype=cpp
" " set tex filetypes
" let g:tex_flavor = "latex"

"" SET VARIABLES
"" auto refresh files
"" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/149214
"" https://stackoverflow.com/questions/2490227/how-does-vims-autoread-work/20418591#20418591
"" autocmd FocusGained,BufEnter * :silent! !
"" autocmd FocusLost,WinLeave * :silent! noautocmd w
"" make scrolling fast (https://stackoverflow.com/questions/307148/vim-scrolling-slowly)
"set ttyfast
" set path for to allows searching for files within current subdirectories
set path=.,/usr/include,,**
"" set delay for ESC
"" set timeoutlen=300 ttimeoutlen=0
" do not create swap file
set noswapfile
"" hightline current line
"" set cursorline
" turn off error beep/flash
set visualbell t_vb=
" turn off visual bell
set novisualbell
" turn on line numbers
set number
"" show command in the last line of the screen
"set showcmd
"" allow hidden buffers, don't limit t o1 file per window/split
"set hidden
" set for (bash like) tab completion of file names
set wildmode=longest,list,full
set wildmenu
" files to ignore when expanding wildcards
set wildignore+=*/tmp/*,*.so,*.swp,*.zip
" set column width marker
set colorcolumn=+1
"" folding settings
set foldmethod=syntax   "fold based on syntax
set foldnestmax=10      "deepest fold is 10 levels
set nofoldenable        "dont fold by default
set foldlevel=1         "this is just what i use
" set wrap size for just programming files
autocmd FileType c,cpp,java,py set textwidth=100  " set hard wrap width
" set format options (default tcq) see http://vimdoc.sourceforge.net/htmldoc/change.html#fo-table
set formatoptions-=t  " don't auto-wrap text
" set correct wrapping for text files
autocmd FileType markdown set wrap
autocmd FileType markdown set linebreak
autocmd FileType markdown set nolist  " list disables linebreak
autocmd FileType markdown set textwidth=0
autocmd FileType markdown set wrapmargin=0
" turn off wrapping for git commits
autocmd FileType gitcommit set wrap
autocmd FileType gitcommit set linebreak
autocmd FileType gitcommit setlocal textwidth=100
autocmd FileType gitcommit set wrapmargin=0
" do indenting
set autoindent
set nosmartindent
set nocindent
" better searching
set ignorecase
set smartcase
set hlsearch
" set the current directory relative to the current buffer
set autochdir
" need to turn autchdir off for quicklist from make to work
set noautochdir
" turn spell check on
set spell
" set opening new windows to the right and bottom
set splitright
set splitbelow
" make the screen scroller faster
set lazyredraw
" prevent vim from clearing clipboard on exit
autocmd VimLeave * call system("xsel -ib", getreg('+'))
" sets up quicklist window after running :make
autocmd QuickFixCmdPost [^l]* nested cwindow
autocmd QuickFixCmdPost    l* nested lwindow

" AUTO COMPLETE
" http://vim.wikia.com/wiki/Make_Vim_completion_popup_menu_work_just_like_in_an_IDE
set completeopt=longest,menuone
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
" open omni completion menu closing previous if open and opening new menu
" without changing the text
inoremap <expr> <C-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
    \ '<C-x><C-o><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>": ""<CR>'
" open user completion menu closing previous if open and opening new menu
" without changing the text
inoremap <expr> <S-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
    \ '<C-x><C-u><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>": ""<CR>

" SET SYNTAX
" highlight extra whitespace (http://vim.wikia.com/wiki/Highlight_unwanted_spaces)
highlight ExtraWhitespace ctermbg=red guibg=red
" use this command before the colorscheme command to ensure the highlight group does not get
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
" " show trailing whitespace
" match ExtraWhitespace /\s\+$/
" " disable underscore markdown error
" " https://github.com/tpope/vim-markdown/issues/21
" syn match markdownError "\w\@<=\w\@="

syntax on
colorscheme monokai
" see https://sunaku.github.io/vim-256color-bce.html for a better solution
set term=screen-256color

" TAB SETTINGS
" convert tabs to spaces
set expandtab
" insert and delete indent correctly
set smarttab
" tab size
set shiftwidth=2
set softtabstop=2
set tabstop=2
" set tab size to 4 for the following file types
autocmd FileType python setlocal shiftwidth=4 softtabstop=4 tabstop=4

" SHORTCUT KEYS
" set leader key
let mapleader=" "

" don't move cursor when highlighting the word the cursor and set current line to middle of screen
nnoremap * viwy/\<<C-R>"\><CR>N
" easy pasting
nnoremap <leader>p "0p
" copy current buffer file full path to copy "0 reg
nnoremap <leader>c :let @* = expand("%:p")<cr>

" fill in current directory path
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
cnoremap <C-A> <Home>
cnoremap <C-B> <Left>
cnoremap <C-E> <End>
cnoremap <C-F> <Right>

" mimic emacs cursor movements in Insert Mode Only
inoremap <C-A> <Home>
inoremap <C-B> <Left>
inoremap <C-E> <End>
inoremap <C-F> <Right>
inoremap <C-P> <Up>
inoremap <C-N> <Down>

" use Tab to tab and keep selection highlighted after tabbing
vmap <Tab> >gv
vmap <S-Tab> <gv

" build keys
" look into getting tpope/dispatch working for this :Make
nnoremap <f2> :make<CR>
nnoremap <silent> <f3> :!clang-format -i %<CR>
nnoremap <silent> <f4> :!ctags -R --exclude=".git/*" --exclude="build/*" --exclude="devel/*" --exclude="venv/*" --exclude="*.egg-info" --exclude="*.so" .<CR>

" SETTINGS vim-fugitive
" auto-clean fugitive buffers
autocmd BufReadPost fugitive://* set bufhidden=delete

" SETTINGS NERDTree
" set tree explore toggle map
nnoremap <leader>j :NERDTreeFind<CR>
nnoremap <leader>k :NERDTreeToggle<CR>
nnoremap <leader>l :NERDTreeFocus<CR>
let NERDTreeIgnore = ['\.aux$','\.bbl$','\.blg$','\.dvi$','\.fdb_latexmk$','\.fls$','\.log$','\.pdf$','\.pyc$','\.sta','\.synctex.gz$','\.toc']

" SETTINGS vim-commentary
" set comment style to // for c and java files
autocmd FileType c,cpp,java setlocal commentstring=//\ %s
" set comment style to # for cmake files
autocmd FileType cmake setlocal commentstring=#\ %s

"" SETTINGS vim-submodes
"" adjust window size (http://ddrscott.github.io/blog/2016/making-a-window-submode/)

"" A message will appear in the message line when you're in a submode
"" and stay there until the mode has existed.
"let g:submode_always_show_submode = 1

"" set for continuous window size adjustment
"for key in ['-', '+', '<', '>']
"    call submode#enter_with('window_adjust', 'n', '', '<C-w>' . key, '<C-w>' . key)
"    call submode#map('window_adjust', 'n', '', key, '<C-w>'. key)
"endfor
"call submode#leave_with('window_adjust', 'n', '', '<ESC>')

" SETTINGS ctrlp
nnoremap <leader>p :CtrlPTag<CR>
let g:ctrlp_match_window = 'results:100'
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]\.(git|hg|svn)$|build|devel|venv',
  \ 'file': '\v\.(exe|so|dll|DS_Store)$',
  \ }

"" SETTINGS CamelCaseMotion
"omap <silent> uw <Plug>CamelCaseMotion_iw
"xmap <silent> uw <Plug>CamelCaseMotion_iw
"omap <silent> ub <Plug>CamelCaseMotion_ib
"xmap <silent> ub <Plug>CamelCaseMotion_ib
"omap <silent> ue <Plug>CamelCaseMotion_ie
"xmap <silent> ue <Plug>CamelCaseMotion_ie

" SETTINGS vim-airline
let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
let g:airline_section_z = '%3p%% %3l/%L:%3v'
let g:airline#extensions#wordcount#format = '%d words'
let g:airline_detect_spell=0
let g:airline_detect_spelllang=0
let g:airline_mode_map = {
      \ '__' : '-',
      \ 'c'  : 'C',
      \ 'i'  : 'I',
      \ 'ic' : 'I',
      \ 'ix' : 'I',
      \ 'n'  : 'N',
      \ 'ni' : 'N',
      \ 'no' : 'N',
      \ 'R'  : 'R',
      \ 'Rv' : 'R',
      \ 's'  : 'S',
      \ 'S'  : 'S',
      \ '' : 'S',
      \ 't'  : 'T',
      \ 'v'  : 'V',
      \ 'V'  : 'V'}
let g:airline_theme='soda'
